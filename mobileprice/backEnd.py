import pickle
import numpy as np
import pandas as pd

model = pickle.load(open('model.pkl', 'rb'))

class backEnd:
    
    def __init__(self, RAM, ROM, Primary_Cam, Selfie_Cam, Battery_Power):
        self.RAM = RAM
        self.ROM = ROM
        self.Primary_Cam = Primary_Cam
        self.Selfie_Cam = Selfie_Cam
        self.Battery_Power = Battery_Power
        self.arrange()

    def arrange(self):
        finalList = [0] * 5
        finalList[0] = self.RAM
        finalList[1] = self.ROM
        finalList[2] = self.Primary_Cam
        finalList[3] = self.Selfie_Cam
        finalList[4] = self.Battery_Power
        
        data = pd.DataFrame({'ROM(GB)': finalList[0], 'RAM(GB)': finalList[1], 'Primary_Cam(MP)': finalList[2], 'Selfi_Cam(MP)': finalList[3], 'Battery_Power(mAh)': finalList[4]},index=[0])
        self.predict(data)

    def predict(self, list): #predict price of mobile given the above dataset

        arrToPredict = np.array([list])
        self.totalPredicted = model.predict(arrToPredict)








from flask import  Flask, render_template,request
from backEnd import backEnd

app = Flask(__name__)

@app.route('/')


def main():
    return render_template('Home.html')

@app.route('/predict', methods=['POST','GET'])

def home():
    ROM = int(request.form['ROM'])
    RAM = int(request.form['RAM'])
    Primary_Cam = int(request.form['Primary_Cam'])
    Selfie_Cam = int(request.form['Selfie_Cam'])
    Battery_Power = int(request.form['Battery_Power'])
    back = backEnd(RAM, ROM, Primary_Cam, Selfie_Cam, Battery_Power)
    final_predicted = (round(float(back.totalPredicted[0]),2))
    return render_template("Home.html", prediction_text="Your mobile price is {}".format(final_predicted))

if __name__ == "__main__":
    app.run(debug=True)
