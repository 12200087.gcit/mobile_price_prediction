import numpy as np
from flask import Flask, render_template, request
import pickle
import pandas as pd
# from flask_navigation import Navigation
# from flask.ext.navigation import Navigation

app = Flask(__name__)
model = pickle.load(open("model.pkl","rb"))
# nav = Navigation(app)

# nav = Navigation()
# nav.init_app(app)

# nav.Bar('top', [
#     # nav.Item('Home', 'home'),
#     nav.Item('mobileprice', 'mobileprice'),
# ])

@app.route('/')
def home():
    return render_template('home.html')

# @app.route('/mobileprice')
# def mobileprice():
#     return render_template('mobileprice.html')

@app.route('/predict',methods=['POST'])
def predict():
    ROM = int(request.form['ROM'])
    RAM = int(request.form['RAM'])
    Primary_Cam = int(request.form['Primary_Cam'])
    Selfie_Cam = int(request.form['Selfie_Cam'])
    Battery_Power = int(request.form['Battery_Power'])
    prediction = model.predict(
        pd.DataFrame({'ROM(GB)': ROM, 'RAM(GB)': RAM, 'Primary_Cam(MP)': Primary_Cam, 'Selfi_Cam(MP)': Selfie_Cam, 'Battery_Power(mAh)': Battery_Power},
        index=[0])

    ) 
   
    output = round(prediction[0], 2)
    return render_template('home.html', prediction_text='Your mobile price is Nu: {}'.format(output))

if __name__ == "__main__":
    app.run(debug=True ,port=5000,use_reloader=False)